export interface Client {
    nom: string;
    email: string; 
    password: string; 
    telephone: string; 
    role: string;
}