import { Injectable, NgZone } from '@angular/core';
import { Client } from 'Models/Client';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {

  headers = { 'content-type': 'application/json'}  

  constructor(
    private http: HttpClient
   ) { }

   AjoutterUtilisateur(objetClient: Client) {
    const body=JSON.stringify(objetClient);
    return this.http.post('http://93.90.207.75:8080/api/Users', body,{'headers':this.headers});
  }

}
