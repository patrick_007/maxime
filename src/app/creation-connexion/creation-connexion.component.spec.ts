import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationConnexionComponent } from './creation-connexion.component';

describe('CreationConnexionComponent', () => {
  let component: CreationConnexionComponent;
  let fixture: ComponentFixture<CreationConnexionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreationConnexionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreationConnexionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
