import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthentificationService } from 'src/Services/auth/authentification.service';

@Component({
  selector: 'app-creation-connexion',
  templateUrl: './creation-connexion.component.html',
  styleUrls: ['./creation-connexion.component.scss']
})
export class CreationConnexionComponent implements OnInit {

  active_tab_one : string | undefined;
  active_tab_two!: string;
  isSearchActiveTab!: boolean;
  listClientes!: [number,number, number];
  IsNotDeployed!: boolean;
  isCardOver!: boolean;
  connectForm!: FormGroup;

  constructor(private formBuilder: FormBuilder, public authserv : AuthentificationService, private router: Router) {
    this.activeTab(1);
    this.initClassVar();
  }

  ngOnInit(): void {
  }


  SeConnecter(groupConection: any) {
    this.authserv.SignIn(groupConection.email, groupConection.password);
  }

  redirectTo(chemin: string) {
    // this.router.navigateByUrl(chemin);
  }

   /**
   * 
   * @param indexTab 
   */
    activeTab(indexTab: number) {
      if(indexTab && indexTab === 1) {
        this.active_tab_one = "active";
        this.active_tab_two = "";
        this.isSearchActiveTab = true;
      } else {
        this.active_tab_one = "";
        this.active_tab_two = "active";
        this.isSearchActiveTab = false;
      }
    }
  
    initClassVar() {
      this.listClientes = [1,1,1];
      this.IsNotDeployed = true;
      this.isCardOver = false;
      this.connectForm = this.formBuilder.group({ 
        email: ['', [Validators.required, Validators.email]],
        password:  ['', [Validators.required, Validators.maxLength(40)]],
      });
    }
  
    changeButtonMenu(indexButton : number) {
      if(indexButton === 1) {
        this.IsNotDeployed = false;
      }
      else {
        this.IsNotDeployed = true;
      }
    }
  
    activateButton() {
      this.isCardOver = true;
    }

}
