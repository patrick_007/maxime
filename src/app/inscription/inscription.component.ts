import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { Client } from 'Models/Client';
import { AuthentificationService } from 'src/Services/auth/authentification.service';
import { UtilisateurService } from 'src/Services/Utilisateur/utilisateur.service';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {

  active_tab_one : string | undefined;
  active_tab_two!: string;
  IsNotDeployed!: boolean;
  isCardOver!: boolean;
  clientForm!: FormGroup;
 


  constructor(public userServ: UtilisateurService, private formBuilder: FormBuilder, public authserv : AuthentificationService, private router: Router) {
    this.initClassVar();
  }

  ngOnInit(): void {
  }


  /**
   * cette fonction permet d'inscrire ou d'ajoutter une CLiente dans la 
   * base de donnée 
   */
  async ajoutterUtilisateur(data: any) {
    const newClient : Client = {
      email: data.email,
      nom: data.nom,
      password: data.password,
      role: 'client',
      telephone: '+237' + data.telephone
    }
    if(this.isValidData(newClient)){
        this.userServ.AjoutterUtilisateur(newClient).subscribe(async (data) => {
          if(data) {
              await this.authserv.SignOut();
              await this.authserv.SignIn(newClient.email, newClient.password);
              await this.redirectTo('acceuil');
          }
        }, error => {
           debugger;
           // traitement des erreurs
        });
    }   
  }

  async isValidData(client: Client) {
      if(client.email === '' || client.email.length === 0) {
        window.alert("rempir le mail");
        return false;
      } else if(client.password === '' || client.password.length === 0) {
        window.alert("rempir le mot de passe");
        return false;
      }
      else if(client.telephone === '') {
        window.alert("vérifiez le numero de téléphone");
        return false;
      } else {
        return true;
      }
     
  }

  redirectTo(chemin: string) {
    // this.router.navigateByUrl(chemin);
  }

    initClassVar() {
      this.IsNotDeployed = true;
      this.isCardOver = false;
      this.clientForm = this.formBuilder.group({ 
        email: ['', [Validators.required, Validators.email]],
        password:  ['', [Validators.required, Validators.maxLength(40)]],
        telephone: ['', [Validators.required]],
        accepted: [true, [Validators.required, Validators.requiredTrue]],
        nom:[''],
        role: ['client']
      });
    }
  
    changeButtonMenu(indexButton : number) {
      if(indexButton === 1) {
        this.IsNotDeployed = false;
      }
      else {
        this.IsNotDeployed = true;
      }
    }
  
    activateButton() {
      this.isCardOver = true;
    }

}
