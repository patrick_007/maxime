import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'wbgmarket3';
  active_tab_one : string | undefined;
  active_tab_two!: string;
  isSearchActiveTab!: boolean;
  listClientes!: [number,number, number];
  IsNotDeployed!: boolean;
  isCardOver!: boolean;
  
  constructor() {
    this.initClassVar();
  }

  initClassVar() {
    this.listClientes = [1,1,1];
    this.IsNotDeployed = true;
    this.isCardOver = false;
  }

  changeButtonMenu(indexButton : number) {
    if(indexButton === 1) {
      this.IsNotDeployed = false;
    }
    else {
      this.IsNotDeployed = true;
    }
  }

 

}
