import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { CreationConnexionComponent } from './creation-connexion/creation-connexion.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AuthentificationService } from 'src/Services/auth/authentification.service';
import { InscriptionComponent } from './inscription/inscription.component';
import { UtilisateurService } from 'src/Services/Utilisateur/utilisateur.service';

@NgModule({
  declarations: [
    AppComponent,
    AcceuilComponent,
    CreationConnexionComponent,
    InscriptionComponent
  ],
  imports: [
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserModule,
    AppRoutingModule,
    AngularFirestoreModule, 
    AngularFireAuthModule, 
    AngularFireStorageModule 
  ],
  providers: [AuthentificationService, UtilisateurService],
  bootstrap: [AppComponent],
  exports: [FormsModule]
})
export class AppModule {
  
    

}
