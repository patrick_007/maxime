import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { CreationConnexionComponent } from './creation-connexion/creation-connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';

const routes: Routes = [
  { 
      path: 'acceuil', 
      component: AcceuilComponent 
  },
  { 
    path: 'connexion-compte', 
    component: CreationConnexionComponent 
  },
  { 
    path: 'inscription', 
    component: InscriptionComponent 
  },
  {
    path:'', 
    redirectTo: 'acceuil',
    pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
